<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chercher les valeurs possibles d'un champ
 * 
 * @param array $options
 *   Tableau des infos nécessaires :
 *   - 
 **/
function rechercher_valeurs_champ($options) {
	$valeurs = array();
	
	if (
		isset($options['table'])
		and isset($options['champ'])
	) {
		// On va chercher toutes les valeurs uniques possibles
		$valeurs_brutes = sql_allfetsel("distinct(${options['champ']})", $options['table']);
		$valeurs_brutes = array_map('reset', $valeurs_brutes);
		
		// Si on sait que ce sont des listes à virgules
		if (isset($options['format_valeur']) and $options['format_valeur'] == 'virgule') {
			$valeurs_brutes = array_map(
				function($valeur) { return array_map('trim', explode(',', $valeur)); },
				$valeurs_brutes
			);
			$valeurs_brutes = call_user_func_array('array_merge', $valeurs_brutes);
		}
		
		// Nettoyage
		$valeurs_brutes = array_filter($valeurs_brutes);
		
		// Par défaut les labels sont les valeurs
		$valeurs = array_combine($valeurs_brutes, $valeurs_brutes);
		
		// Maintenant on cherche les labels…
		
		// Si on trouve les labels directement dans les options
		if (isset($options['labels']) and is_array($options['labels'])) {
			foreach ($valeurs as $cle=>$label) {
				if (isset($options['labels'][$cle])) {
					$valeurs[$cle] = $options['labels'][$cle];
				}
			}
		}
		// Sinon si la source des labels est dans une table, on part du principe que c'est une table qui a des titres
		if (isset($options['labels_source']) and $options['labels_source'] == 'table' and isset($options['labels_table'])) {
			$labels_table = table_objet_sql($options['labels_table']);
			$cle_table = id_table_objet($labels_table);
			
			// Si on a donné le champ où trouver le label
			if (isset($options['labels_champ_label'])) {
				$labels_champ_label = $options['labels_champ_label'];
			}
			// Sinon c'est "titre" par défaut
			else {
				$labels_champ_label = 'titre';
			}
			
			// Si on a donné le champ où chercher les valeurs brutes
			if (isset($options['labels_champ_valeur'])) {
				$labels_champ_valeur = $options['labels_champ_valeur'];
			}
			// Sinon c'est la primary de la table
			else {
				$labels_champ_valeur = $cle_table;
			}
			
			// On a toutes les infos pour faire la requête
			if ($labels = sql_allfetsel(
				array($cle_table, $labels_champ_valeur, $labels_champ_label),
				$labels_table,
				sql_in($labels_champ_valeur, $valeurs_brutes)
			)) {
				foreach ($labels as $ligne) {
					if (isset($valeurs[$ligne[$labels_champ_valeur]])) {
						// On applique le traitement prédéfini pour ce champ de cette table
						$valeurs[$ligne[$labels_champ_valeur]] = appliquer_traitement_champ(
							$ligne[$labels_champ_label],
							$labels_champ_label,
							table_objet($labels_table),
							array('id_objet' => $ligne[$cle_table], 'objet' => objet_type($labels_table), '')
						);
					}
				}
			}
			
			// On trie
			asort($valeurs);
		}
	}
	
	return $valeurs;
}

