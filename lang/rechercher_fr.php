<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'resultats_1' => 'Un résultat',
	'resultats_aucun' => 'Aucun résultat',
	'resultats_nb' => '@nb@ résultats',
);
